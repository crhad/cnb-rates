# Usage
 
Run with `docker-compose up` in the solution directory 

# API 
 
#### Pulling data from CNB 
`POST` request to `/api/rates` with empty body 

#### Get all stored currency information 
`GET` request to `/api/rates` with empty body 

#### Get currency information about a specific currency 
`GET` request to `/api/rates/[CODE]`, for example `/api/rates/USD` 

#### Convert between two currencies
`GET` request to `/api/converter?fromCode=[CODE]&toCode=[CODE]&amount=[AMOUNT]`, for example `/api/converter?fromCode=AUD&toCode=USD&amount=10`
 
 
# Notes 
 
By default, the API is available at `localhost:5101` 
 
Logs can be accessed by any mongo client, they are stored in the `logs` database, exposed through the 27017 port 

All data accessed via the API is in JSON, logs are also in JSON