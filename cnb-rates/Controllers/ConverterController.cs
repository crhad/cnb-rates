﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cnb_rates.Models;
using cnb_rates.Utils;
using Microsoft.AspNetCore.Mvc;
using Serilog;


namespace cnb_rates.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ConverterController : Controller
    {
        QueryHandler qh;

        // GET: api/converter?fromCode=[CODE]&toCode=[CODE]&amount=[AMOUNT]
        // returns information about currency rate for the given currency code
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] string fromCode, [FromQuery] string toCode, [FromQuery] double amount)
        {
            Log.Logger.Information("Converting from {FromCode} to {ToCode} with amount {Amount}", fromCode, toCode, amount);
            var fromInfo = await qh.GetRateAsync(fromCode);
            var toInfo = await qh.GetRateAsync(toCode);
            if (fromInfo == null || toInfo == null)
            {
                Log.Logger.Error("Converting from {FromCode} to {ToCode} with amount {Amount} failed: currency not found", fromCode, toCode, amount);
                return BadRequest("Currency not found");
            }
            if (amount < 0)
            {
                Log.Logger.Error("Converting from {FromCode} to {ToCode} with amount {Amount} failed: invalid amount", fromCode, toCode, amount);
                return BadRequest("Invalid conversion amount");
            }
            return Ok(Converter.Convert(fromInfo, toInfo, amount));
        }

        public ConverterController(QueryHandler queryHandler)
        {
            qh = queryHandler;
        }
    }
}
