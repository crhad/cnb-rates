﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using cnb_rates.Models;
using Serilog;
using System.Net;

namespace cnb_rates.Controllers
{
    [Produces("application/json")]
    [Route("api/Rates")]
    public class RatesController : Controller
    {
        CommandHandler ch;
        QueryHandler qh;

        // GET: api/Rates
        // returns all information about the current rates
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            Log.Logger.Information("Retrieving all rates info");
            var rates = await qh.GetRatesAsync(); // If no info is found, that is a valid result -> return empty collection
            return Ok(rates);
        }

        // GET: api/Rates/[CODE]
        // returns information about currency rate for the given currency code
        [HttpGet("{code}", Name = "Get")]
        public async Task<IActionResult> Get(string code)
        {
            Log.Logger.Information("Retrieving information about {CurrencyCode}", code);
            var rateInfo = await qh.GetRateAsync(code);
            if (rateInfo == null)
            {
                Log.Logger.Error("Retrieving information about {CurrencyCode} failed: currency was not found", code);
                return BadRequest("Currency not found");
            }

            return Ok(rateInfo);
        }

        // POST: api/Rates
        // instructs the application to update its currency database from the CNB site
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]string value)
        {
            Log.Logger.Information("Pulling new data from CNB site");
            try
            {
                await ch.UpdateAsync();
            }
            catch (WebException ex)
            {
                Log.Logger.Error("Could not retrieve currency data: {Exception}", ex);
                return StatusCode(500, "Could not retrieve currency data");
            }
            return Ok("Successfully retrieved currency data");
        }

        public RatesController(CommandHandler commandHandler, QueryHandler queryHandler)
        {
            ch = commandHandler;
            qh = queryHandler;
        }
    }
}
