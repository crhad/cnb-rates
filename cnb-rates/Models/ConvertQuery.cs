﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cnb_rates.Models
{
    public class ConvertInfo
    {
        public string FromCode { get; set; }
        public string ToCode { get; set; }
        public double Amount { get; set; }
        public double Result { get; set; }
    }
}
