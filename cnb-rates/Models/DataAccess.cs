﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using cnb_rates.Utils;
using MongoDB.Driver;

namespace cnb_rates.Models
{
    // Base class for Command and Query handlers
    public abstract class DataAccessBase
    {
        protected IMongoClient client;
        protected IMongoDatabase database;

        public DataAccessBase()
        {
            client = new MongoClient(Utils.Constants.MongoConnectionString);
            database = client.GetDatabase(Utils.Constants.DocumentDB);
        }
    }

    public class QueryHandler : DataAccessBase
    {
        // Get all available currency data (does not pull new data! only uses the data already in the database)
        public RateCollection GetRates()
        {
            var origin = database.GetCollection<OriginInfo>(Utils.Constants.OriginCollection).Find(_ => true).FirstOrDefault();
            var rates = database.GetCollection<RateInfo>(Utils.Constants.RatesCollection).Find(_ => true).ToList();
            return new RateCollection { Origin = origin, Rates = rates };
        }

        public async Task<RateCollection> GetRatesAsync()
        {
            var origin = await database.GetCollection<OriginInfo>(Utils.Constants.OriginCollection).FindAsync(_ => true);
            var actualOrigin = await origin.FirstOrDefaultAsync();
            var rates = await database.GetCollection<RateInfo>(Utils.Constants.RatesCollection).FindAsync(_ => true);
            var rateList = await rates.ToListAsync();
            return new RateCollection { Origin = actualOrigin, Rates = rateList };
        }

        // Get information about a single currency's rate
        public RateInfo GetRate(string code)
        {
            return database.GetCollection<RateInfo>(Utils.Constants.RatesCollection).Find(x => x.Code == code).FirstOrDefault();
        }

        public async Task<RateInfo> GetRateAsync(string code)
        {
            var result = await database.GetCollection<RateInfo>(Utils.Constants.RatesCollection).FindAsync(x => x.Code == code);
            return await result.FirstOrDefaultAsync();
        }
    }

    public class CommandHandler : DataAccessBase
    {
        // Pull new currency data from the CNB site and replace existing data with the new set
        public void Update()
        {
            var collection = GetCurrencyData();
            var origin = database.GetCollection<OriginInfo>(Utils.Constants.OriginCollection);
            var rates = database.GetCollection<RateInfo>(Utils.Constants.RatesCollection);

            origin.DeleteMany(_ => true);
            rates.DeleteMany(_ => true);
            origin.InsertOne(collection.Origin);
            rates.InsertMany(collection.Rates);
        }

        public async Task UpdateAsync()
        {
            var collection = GetCurrencyData();
            var origin = database.GetCollection<OriginInfo>(Utils.Constants.OriginCollection);
            var rates = database.GetCollection<RateInfo>(Utils.Constants.RatesCollection);
            await Task.WhenAll(new List<Task>
            {
                UpdateOriginAsync(origin, collection),
                UpdateRatesAsync(rates, collection)
            });

        }

        private RateCollection GetCurrencyData()
        {
            var client = new WebClient();
            RateCollection baseRates, otherRates;
            baseRates = Parser.GetCollection(client, Utils.Constants.CurrencySource);
            otherRates = Parser.GetCollection(client, Utils.Constants.AlternateCurrencySource);
            return Converter.Merge(baseRates, otherRates);
        }

        private async Task UpdateOriginAsync(IMongoCollection<OriginInfo> origin, RateCollection collection)
        {
            await origin.DeleteManyAsync(_ => true);
            await origin.InsertOneAsync(collection.Origin);
        }

        private async Task UpdateRatesAsync(IMongoCollection<RateInfo> rates, RateCollection collection)
        {
            await rates.DeleteManyAsync(_ => true);
            await rates.InsertManyAsync(collection.Rates);
        }
    }
}
