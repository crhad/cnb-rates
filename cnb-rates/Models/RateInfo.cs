﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace cnb_rates.Models
{
    public class RateCollection
    {
        public OriginInfo Origin { get; set; }
        public IEnumerable<RateInfo> Rates { get; set; }
    }

    public class OriginInfo
    {
        [JsonIgnore]
        public string Id { get; set; }
        [BsonElement("OriginTime")]
        public DateTime OriginTime { get; set; }
        [BsonElement("VersionInfo")]
        public string VersionInfo { get; set; }
    }

    public class RateInfo
    {
        [JsonIgnore]
        public string Id { get; set; }
        [BsonElement("Country")]
        public string Country { get; set; }
        [BsonElement("Currency")]
        public string Currency { get; set; }
        [BsonElement("Amount")]
        public int Amount { get; set; }
        [BsonElement("Code")]
        public string Code { get; set; }
        [BsonElement("Rate")]
        public double Rate { get; set; }
    }
}
