﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cnb_rates.Utils
{
    public static class Constants
    {
        public static Uri CurrencySource => new Uri("https://www.cnb.cz/en/financial_markets/foreign_exchange_market/exchange_rate_fixing/daily.txt");
        public static Uri AlternateCurrencySource => new Uri("http://www.cnb.cz/en/financial_markets/foreign_exchange_market/other_currencies_fx_rates/fx_rates.txt");
        public static string MongoConnectionString => Environment.GetEnvironmentVariable("DB_CONNECTION_STRING");
        public static string DocumentDB => Environment.GetEnvironmentVariable("DB_DOCUMENT");
        public static string LogDB => Environment.GetEnvironmentVariable("DB_LOG");
        public static string OriginCollection => Environment.GetEnvironmentVariable("DB_COLLECTION_ORIGIN");
        public static string RatesCollection => Environment.GetEnvironmentVariable("DB_COLLECTION_RATES");
        public static int LogSizeLimit => 50; // MB
        public static int LogCountLimit => 1000; // Entries
    }
}
