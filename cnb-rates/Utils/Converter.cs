﻿using cnb_rates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cnb_rates.Utils
{
    public static class Converter
    {
        // Convert between two currencies
        public static ConvertInfo Convert(RateInfo from, RateInfo to, double amount)
        {
            return new ConvertInfo
            {
                Amount = amount,
                FromCode = from.Code,
                ToCode = to.Code,
                Result = amount * from.Rate / from.Amount / to.Rate * to.Amount
            };
        }

        public static RateCollection Merge(RateCollection x, RateCollection y)
        {
            var result = new RateCollection
            {
                // If the collections are from different times, the new collection will have the same age as the newer of the two
                Origin = x.Origin.OriginTime > y.Origin.OriginTime ? x.Origin : y.Origin,
                Rates = x.Rates.Concat(y.Rates)
            };
            return result;
        }
    }
}
