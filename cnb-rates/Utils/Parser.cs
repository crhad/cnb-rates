﻿using cnb_rates.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace cnb_rates.Utils
{
    public static class Parser
    {
        // For giving unique int IDs to currencies for mongo
        private static class IdDistributor
        {
            private static int Id { get; set; } = 0;

            public static int GetId()
            {
                ++Id;
                return Id;
            }
        }

        // Parses the CNB rates text document into a RateCollection
        public static RateCollection ParseRates(StreamReader reader)
        {
            var collection = new RateCollection();
            var list = new List<RateInfo>();

            var header = reader.ReadLine();
            var headerTokens = header.Split(" ");
            var originInfo = new OriginInfo
            {
                VersionInfo = headerTokens[2]
            };
            var dateString = headerTokens[0] + headerTokens[1];
            originInfo.OriginTime = DateTime.Parse(dateString);
            collection.Origin = originInfo;

            string line; RateInfo info;
            reader.ReadLine();
            while (!reader.EndOfStream)
            {
                line = reader.ReadLine();
                info = ParseRate(line);
                info.Id = IdDistributor.GetId().ToString();
                list.Add(info);
            }

            collection.Rates = list;
            return collection;
        }

        // Parse CNB format rate info to a RateInfo
        public static RateInfo ParseRate(string s)
        {
            var info = new RateInfo();
            var tokens = s.Split('|');
            info.Country = tokens[0];
            info.Currency = tokens[1];
            info.Amount = int.Parse(tokens[2]);
            info.Code = tokens[3];
            info.Rate = double.Parse(tokens[4]);
            return info;
        }

        public static RateCollection GetCollection(WebClient client, Uri uri)
        {
            using (var stream = client.OpenRead(uri))
            {
                var reader = new StreamReader(stream);
                return ParseRates(reader);
            }
        }
    }
}
